/// image-url.js
/// alias imgurl.js
// example.com##+js(imgurl,[href],[selector])
(()=>{
'use strict';
          let needle = '{{1}}';
          if ( needle === '' || needle === '{{1}}' ) {
              needle = '^';
          } else if ( needle.slice(0,1) === '/' && needle.slice(-1) === '/' ) {
              needle = needle.slice(1,-1);
          } else {
              needle = needle.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
          }
          needle = new RegExp(needle);
	  const imgurlode = () => {
		  			const nodes = document.querySelectorAll('{{2}}');
		  			try {
                                                  for (const node of nodes) {
							
                                                            node.src = needle; 
						       	    
                                                  }
                                        } catch { }
          };
	  const observer = new MutationObserver(imgurlnode);
    	  observer.observe(document.documentElement, { childList: true, subtree: true });
	  if ( document.readyState === "complete" ) { observer.disconnect(); }
})();
